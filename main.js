// Екранування символів - це заміна символів на текст символ екрануваня повідомляє програмі що його потрібно сприймати буквально 
// декларація експерсія та стрілкові 
// Підйом, коли при оголошенні функції інтерпретатор знає заздалегідь про всі локальні змінні цієї функції

  
  
function createNewUser() {
  let firstName = prompt("Введіть ваше імя");
  let lastName = prompt("Введіть вашу фамілію");
  const birthday = prompt("Введіть ваш день народження", "dd.mm.yyyy")
  
  const newUser = Object.create(
      {
          getLogin() {
              return (this.firstName.charAt(0) + this.lastName).toLowerCase();
          },
          getAge() {
              const currentdate = new Date();
              const userBirthA = this.birthday.split('.');
              const userBirthD = +userBirthA[0];
              const userBirthM = +userBirthA[1];
              const userBirthY = +userBirthA[2];

              const fullDay = currentdate.getDate();
              const fullMonth = (currentdate.getMonth() + 1);
              const fullYear = currentdate.getFullYear();

              let floatYear = -1;

              if (fullMonth > userBirthM) {
                  floatYear = 0;
              }
              else if (userBirthM === fullMonth) {
                  if (fullDay >= userBirthD) {
                      floatYear = 0;
                  }
              }
              return fullYear - userBirthY + floatYear;
          },
          getPassword() {

              const userBirthA = this.birthday.split('.');
              const userBirthY = +userBirthA[2];
              return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + userBirthY);
          },
      },
      {
          firstName: {
              value: firstName,
              writable: false,
              configurable: true,
          },
          lastName: {
              value: lastName,
              writable: false,
              configurable: true,
          },
          birthday: {
              value: birthday
          }
      }
  );
  return newUser;
}

const user = createNewUser();

console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
